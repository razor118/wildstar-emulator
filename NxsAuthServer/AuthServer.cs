﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using NxsEmu;
using NxsEmu.Game;
using NxsEmu.Network;
using NxsEmu.Network.GamePackets;
using NxsEmu.Cryptography;

namespace NxsAuthServer
{
    class AuthServer : GameServer<AuthClient>
    {
        public AuthServer(ushort port)
            : base(port) { }

        // AuthServer apparently uses a key that changes every patch.
        private readonly static PacketCrypt s_packetCrypt
            = new PacketCrypt(PacketCrypt.GetKeyFromBuildAndCRC());
            //= new PacketCrypt(0xF79BE22ABA73C47B);
            //= new PacketCrypt(0x4E49EBFEDF603F15);

        /*protected override void ClientConnected(AuthClient client)
        {
            Logs.Log("AuthClient connected.");

            // We should send SHello_Msg here because all GameServers use it.
            // NOTE: It is possible that realm server need some more fields or something.
            SHello pkt = new SHello();
            pkt.buildNumber = Constants.Build;
            pkt.networkMessageCRC = Constants.CRC;
            pkt.connectionType = 3; // what's that, does it need to change between auth and realm?
            //client.SendGamePacket(pkt);
            client.SendGamePacketWrapped(pkt);
        }*/

        protected override AuthClient CreateClient(Socket socket)
        {
            // Looks like Auth's packetcrypt is completely static, changing each patch.
            return new AuthClient(socket, s_packetCrypt);
        }

        protected override void DispatchMessage(AuthClient client, GamePacketReader reader, GamePacketMsg msg)
        {
            Logs.Log(LogType.Auth, "Dispatching message {0}.", msg);

            switch (msg)
            {
                case GamePacketMsg.CAuthRequest:
                    /*SAuthError reply = new SAuthError();
                    reply.code = 0xD; // no ws access
                    reply.code2 = 0;
                    //SAuthAccept reply = new SAuthAccept();
                    client.SendGamePacketWrapped(reply);*/

                    client.SendGamePacketWrapped(new SAuthAccept());

                    SGatewayInfo pkt = new SGatewayInfo();
                    pkt.gatewayAddress = BitConverter.ToUInt32(new Byte[] { 1, 0, 0, 127 }, 0);
                    pkt.gatewayPort = 24000;
                    pkt.gatewayTicket = new byte[16] { 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
                    //pkt.gatewayTicket = new byte[16] { 0xA3, 0x40, 0x3E, 0xA5, 0x12, 0xD5, 0x46, 0x4C, 0xB3, 0x81, 0xE6, 0xFE, 0x8D, 0x3C, 0x31, 0xD2 };
                    pkt.accountId = 0;
                    pkt.realmName = "Sandbox";
                    client.SendGamePacketWrapped(pkt);

                    break;
                
            }
        } 

        protected override GamePacketMsg WrapperMsgId
        {
            get { return GamePacketMsg.CClientToAuthWrapper; }
        }
    }
}
