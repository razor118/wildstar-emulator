﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NxsEmu.Network.GamePackets;

namespace NxsAuthServer
{
    [PacketDescription(GamePacketMsg.SAuthError)]
    public class SAuthError : GamePacket<SAuthError>
    {
        [Field(32)]
        public uint code;
        [Field(32)]
        public uint code2;
    }

    [PacketDescription(GamePacketMsg.SAuthAccept)]
    public class SAuthAccept : GamePacket<SAuthAccept>
    { }

    [PacketDescription(GamePacketMsg.SGatewayInfo)]
    public class SGatewayInfo : GamePacket<SGatewayInfo>
    {
        [Field(32)]
        public UInt32 gatewayAddress;
        [Field(16)]
        public UInt16 gatewayPort;
        [ArrayField(16), Field(8)]
        public Byte[] gatewayTicket;
        [Field(32)]
        public UInt32 accountId;
        [StringField(true)]
        public String realmName;
        [Field(2)]
        public UInt32 unkField;
    }
}
