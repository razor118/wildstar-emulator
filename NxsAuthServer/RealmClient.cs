﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using NxsEmu;
using NxsEmu.Network;
using NxsEmu.Network.GamePackets;
using NxsEmu.Cryptography;

namespace NxsAuthServer
{
    class RealmClient : GameClient
    {
        public RealmClient(Socket socket, PacketCrypt packetCrypt)
            : base(socket, packetCrypt) { }

        protected override NxsEmu.Network.GamePackets.GamePacketMsg WrapperMsgId
        {
            get { return GamePacketMsg.SGatewayToClientWrapper; }
        }
    }
}
