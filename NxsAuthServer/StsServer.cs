﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using NxsEmu;
using NxsEmu.Network;
using NxsEmu.Network.StsCommands;
using NxsEmu.Cryptography;

namespace NxsAuthServer
{
    sealed class StsServer : BasicServer<StsClient>
    {
        public StsServer(ushort port)
            : base(port) { }

        protected override StsClient CreateClient(Socket socket)
        {
            return new StsClient(socket);
        }

        protected override void ClientConnected(StsClient client)
        {
            Logs.Log(LogType.Sts, "New client connected.");
        }

        protected override void ReceiveMessage(StsClient client, MemoryStream packet)
        {
            Byte[] buf = packet.GetBuffer();

            if (client.CryptIn != null)
                client.CryptIn.EncryptBuffer(packet.GetBuffer(), 0, packet.Length);

            /* The protocol is basically HTTP and respects the rules of HTTP/1.1
             * I'm trying to make this as simple as possible and the code is trying to detect errors.
             *  instead of attempting to handle invalid or corrupted data.
             * I am not absolutely certain of the encoding used.
             */
            using (StreamReader reader = new StreamReader(packet, Encoding.UTF8))
            {
                if (client.PendingCommand == null)
                {
                    string[] requestLine = reader.ReadLine().Split(' ');
                    if (requestLine.Length == 3)
                    {
                        string method = requestLine[0];

                        if (method == "POST")
                        {
                            string command = requestLine[1];
                            string type = requestLine[2];
                            //Logs.Log("NCAuthServer received message: {0}, {1}, {2}", method, command, type);

                            // get headers, there may be more.
                            int l = 0;
                            int s = 0; // "status" index of message

                            string line = reader.ReadLine();
                            while (!String.IsNullOrEmpty(line) && !reader.EndOfStream)
                            {
                                string[] header = line.Split(new char[] { ':' }, 2);
                                //Logs.Log("{0} : {1}", header[0], header[1]);

                                switch (header[0])
                                {
                                    case "l":
                                        l = int.Parse(header[1]);
                                        break;
                                    case "s":
                                        // NOTE: Could this be more than just a number? Server sends #R.
                                        s = int.Parse(header[1]);
                                        break;
                                    default:
                                        Logs.Log(LogType.Auth, LogLevel.Warning, "Unknown header in message: {0}", line);
                                        break;
                                }

                                line = reader.ReadLine();
                            }

                            client.LastRequestId = s;

                            // NOTE: It could split message in more than 2 packets, I don't know.

                            if (reader.Peek() == -1)
                                if (l == 0)
                                    // dispatch this message.
                                    DispatchCommand(client, command, reader);
                                else
                                    // wait for the rest of the message, save pending command.
                                    client.PendingCommand = command;
                            else
                                // contains message
                                DispatchCommand(client, command, reader);
                        }
                        else
                        {
                            Logs.Log(LogType.Auth, LogLevel.Warning,
                                "StsServer received unhandled request method: {0}", method);
                        }
                    }
                    else
                    {
                        Logs.Log(LogType.Auth, LogLevel.Warning,
                                "StsServer received invalid request line: {0}", requestLine);
                    }
                }
                else
                {
                    DispatchCommand(client, client.PendingCommand, reader);
                    client.PendingCommand = null;
                }
            }
        }

        private void DispatchCommand(StsClient client, string command, StreamReader reader)
        {
            Logs.Log(LogType.Sts, "Dispatching '{0}'", command);

            // NOTE: We maybe could/should use handlers for these, but I don't see any need
            //  for that now.
            switch (command)
            {
                /*case "/Sts/Ping":
                    break;*/
                case "/Sts/Connect":
                    HandleStsConnect(client, reader);
                    break;
                case "/Auth/LoginStart":
                    HandleAuthLoginStart(client, reader);
                    break;
                case "/Auth/KeyData":
                    HandleAuthKeyData(client, reader);
                    break;
                case "/Auth/LoginFinish":
                    HandleAuthLoginFinish(client, reader);
                    break;
                case "/GameAccount/ListMyAccounts":
                    HandleGameAccountListMyAccounts(client, reader);
                    break;
                case "/Auth/RequestGameToken":
                    HandleAuthRequestGameToken(client, reader);
                    break;
            }
        }

        private void HandleStsConnect(StsClient client, StreamReader reader)
        {
            if (client.CurrentStatus != StsClientStatus.None)
            {
                Logs.Log(LogType.Auth, LogLevel.Error, "Client {0} sent StsConnect but is already in another state.");
                client.Close();
                return;
            }

            StsConnect request = new StsConnect();
            using (XmlReader xmlReader = XmlReader.Create(reader))
                request.ReadFrom(xmlReader);

            Logs.Log("StsConnect: {0} {1} {2}", request.Build, request.Process, request.Address);

            client.CurrentStatus = StsClientStatus.Connected;
        }

        private void HandleAuthLoginStart(StsClient client, StreamReader reader)
        {
            // Test, there's is a possibility that it was already connected but is re-trying.
            /*if (client.CurrentStatus != StsClientStatus.Connected)
            {
                Logs.Log(LogType.Auth, LogLevel.Error, "Client {0} sent AuthLoginStart but is in invalid state.");
                client.Close();
                return;
            }*/

            AuthLoginStartRequest request = new AuthLoginStartRequest();
            using (XmlReader xmlReader = XmlReader.Create(reader))
                request.ReadFrom(xmlReader);

            Logs.Log(" LoginStart From: {0} / {1}", request.NetAddress, request.LoginName);

            using (MemoryStream keyDataStream = new MemoryStream(4 + 8 + 4 + 128))
            using (BinaryWriter keyDataWriter = new BinaryWriter(keyDataStream))
            {
                try
                {
                    client.Srp = new SRP6();
                    client.Srp.ReceiveLoginStartInfo(request.LoginName, "123", keyDataWriter);
                }
                catch (SRP6InvalidStateException ex)
                {
                    // This is an issue we don't want to recover from. Log it and close client connection.
                    Logs.Log(LogType.Sts, LogLevel.Error, ex.Message);
                    client.Close();
                    return;
                }

                //Byte[] data = keyDataStream.GetBuffer();
                //Logs.Log("Sending first key (len: {0:X2}):", data.Length);
                //Logs.LogBuffer(data);

                AuthLoginStartReply reply = new AuthLoginStartReply();
                reply.KeyData = Convert.ToBase64String(keyDataStream.GetBuffer());
                client.SendOkReply(reply);
            }

            client.CurrentStatus = StsClientStatus.LoginStart;
        }

        private void HandleAuthKeyData(StsClient client, StreamReader reader)
        {
            if (client.CurrentStatus != StsClientStatus.LoginStart)
            {
                Logs.Log(LogType.Auth, LogLevel.Error, "Client {0} sent AuthKeyData but is in invalid state.");
                return;
            }

            AuthKeyDataRequest request = new AuthKeyDataRequest();
            using (XmlReader xmlReader = XmlReader.Create(reader))
                request.ReadFrom(xmlReader);

            using (MemoryStream clientKeyDataStream = new MemoryStream(Convert.FromBase64String(request.KeyData)))
            using (BinaryReader clientKeyDataReader = new BinaryReader(clientKeyDataStream))
            using (MemoryStream serverKeyDataStream = new MemoryStream(4 + 32))
            using (BinaryWriter serverKeyDataWriter = new BinaryWriter(serverKeyDataStream))
            {
                byte[] key;

                try
                {
                    client.Srp.ReceiveClientProof(clientKeyDataReader, serverKeyDataWriter, out key);
                }
                catch (SRP6InvalidStateException ex)
                {
                    // This is an issue we don't want to recover from. Log it and close client connection.
                    Logs.Log(LogType.Sts, LogLevel.Error, ex.Message);
                    client.Close();
                    return;
                }
                catch (SRP6SafeguardException ex)
                {
                    // This could just mean that the password was wrong, don't even log it.
                    Logs.Log(LogType.Sts, LogLevel.Warning, ex.Message); // to be removed eventually.
                    client.SendErrorReply("ErrBadParam", "<Error code=\"10\"/>");
                    client.CurrentStatus = StsClientStatus.Connected;
                    return;
                }
                finally
                {
                    // Let GC take the mem back.
                    client.Srp = null;
                }

                AuthKeyDataReply reply = new AuthKeyDataReply();
                reply.KeyData = Convert.ToBase64String(serverKeyDataStream.GetBuffer());

                Byte[] data = serverKeyDataStream.GetBuffer();
                Logs.Log("Sending verif key (len: {0:X2}):", data.Length);
                //Logs.LogBuffer(data);

                client.SendOkReply(reply);

                // new RC4: Filter
                //client.Filter = new RC4Filter(key);
                client.CryptIn = new RC4Crypt(key);
                client.CryptOut = new RC4Crypt(key);

                client.CurrentStatus = StsClientStatus.ReceivedKeyData;
            }
        }

        private void HandleAuthLoginFinish(StsClient client, StreamReader reader)
        {
            if (client.CurrentStatus != StsClientStatus.ReceivedKeyData)
            {
                Logs.Log(LogType.Auth, LogLevel.Error, "Client {0} sent AuthLoginFinish but is in invalid state.");
                return;
            }

            using (MemoryStream stream = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.AutoFlush = true;
                writer.Write("<Reply>");
                writer.Write("<UserId></UserId>");
                writer.Write("<UserCenter>3</UserCenter>");
                writer.Write("<Roles type=\"array\"/>");
                writer.Write("<LocationId></LocationId>"); // AF228A31-0230-4212-9E55-6E405728B795
                writer.Write("<AccessMask></AccessMask>");
                writer.Write("<UserName>Sandbox</UserName>");
                writer.Write("</Reply>");
                writer.Flush();

                client.SendOkReplyStream(stream);
            }

            client.CurrentStatus = StsClientStatus.LoginFinish;
        }

        private void HandleGameAccountListMyAccounts(StsClient client, StreamReader reader)
        {
            using (MemoryStream stream = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.AutoFlush = true;
                writer.Write("<Reply type=\"array\">");
                writer.Write("<GameAccount>");
                writer.Write("<Alias></Alias>");
                writer.Write("<Created></Created>");
                writer.Write("</GameAccount>");
                writer.Write("</Reply>");
                writer.Flush();

                client.SendOkReplyStream(stream);
            }
        }

        private static void HandleAuthRequestGameToken(StsClient client, StreamReader reader)
        {
            using (MemoryStream stream = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.AutoFlush = true;
                writer.Write("<Reply><Token>FFFFFFFF-FFFF-FFF-FFFF-FFFFFFFFFFFF</Token></Reply>"); //0D4DE33B-F314-4237-BB41-8D2E01210811
                writer.Flush();

                client.SendOkReplyStream(stream);
            }
        }
    }
}
