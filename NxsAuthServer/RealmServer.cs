﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using NxsEmu;
using NxsEmu.Game;
using NxsEmu.Game.Gateway;
using NxsEmu.Network;
using NxsEmu.Network.GamePackets;
using NxsEmu.Cryptography;

namespace NxsAuthServer
{
    class RealmServer : GameServer<RealmClient>
    {
        public RealmServer(UInt16 port)
            : base(port) { }

        private static PacketCrypt s_initialCrypt =
            new PacketCrypt(PacketCrypt.GetKeyFromBuildAndCRC());

        // NOTE: This is the temporary static ticket.
        private Byte[] s_temporaryTicket = 
            new Byte[16] { 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };

        protected override RealmClient CreateClient(Socket socket)
        {
            // The realmserver uses the same PacketCrypt as Auth for the first few packets (hopefully not more?)
            return new RealmClient(socket, s_initialCrypt);
        }

        protected override void DispatchMessage(RealmClient client, GamePacketReader reader, GamePacketMsg msg)
        {
            Logs.Log(LogType.Realm, "Dispatching realm message id:{0:X4}.", (ushort)msg);

            switch (msg)
            {
                case GamePacketMsg.CGatewayConnect:
                    // empty packet, sent by client right after the token stuff.
                    // TODO: Send the response(s)

                    SCharactersList charactersList = new SCharactersList();
                    charactersList.unknown = 0;
                    charactersList.characterCount = 1;

                    CharacterListEntry c = new CharacterListEntry();
                    c.characterId = 1;
                    c.characterName = "Drakefish";
                    c.charactedPath = 1;
                    c.characterSex = 0;
                    c.characterRace = 12;
                    c.characterClass = 4;
                    c.characterFaction = 166;
                    c.characterLevel = 50;
                    c.charactedPath = 2;

                    c.unk0 = 1634;
                    c.unk1 = 1684;
                    c.unk2 = 107;
                    c.unk3 = 0;

                    c.worldLocation.tileId = 0xFFFFFFFF;
                    c.unk4 = 0;
                    c.unk5 = 0xFFFFFFFF;

                    c.unkCount = 7;
                    c.unkArray0 = new UInt32[7];
                    c.unkArray1 = new UInt32[7];
                    
                    charactersList.characterInfo = new CharacterListEntry[1];
                    charactersList.characterInfo[0] = c;

                    charactersList.additionalCount = 0;
                    client.SendGamePacketWrapped(charactersList);

                    break;
                case GamePacketMsg.CGatewayHello:
                    Logs.Log("Received pkt:");
                    CGatewayRequest request = new CGatewayRequest();
                    request.ReadFrom(reader);

                    // TODO: validate request.

                    // this is at the right place, the next packet is encrypted with it.
                    client.PacketCrypt = new PacketCrypt(PacketCrypt.GetKeyFromTicket(request.gatewayTicket));
                    break;
                case GamePacketMsg.CJoinWorld:
                    SJoinWorld1 join1 = new SJoinWorld1();
                    join1.unknown = 0x662;
                    //join1.worldLocation.pitch = 0;
                    //join1.worldLocation.tileId = 0;
                    //join1.worldLocation.yaw = 0;
                    client.SendGamePacketWrapped(join1);

                    SJoinWorldUnk2 join2 = new SJoinWorldUnk2();
                    join2.unk0 = 0xCDED;
                    join2.unk1 = 0x4;
                    join2.unk2 = 0x3138;
                    client.SendGamePacketWrapped(join2);

                    SJoinWorldUnk3 join3 = new SJoinWorldUnk3();
                    client.SendGamePacketWrapped(join3);

                    SJoinWorldUnk4 join4 = new SJoinWorldUnk4();
                    client.SendGamePacketWrapped(join4);

                    SJoinWorldUnk5 join5 = new SJoinWorldUnk5();
                    join5.unk0 = 0x00040001;
                    join5.unk1 = 1;
                    client.SendGamePacketWrapped(join5);

                    SJoinWorldUnk6 join6 = new SJoinWorldUnk6();
                    join6.unk3 = 0x64A4;
                    join6.unk4 = 0x4;
                    client.SendGamePacketWrapped(join6);

                    SCreatureSpawnInfo spawn = new SCreatureSpawnInfo();
                    spawn.unitId = 1;
                    spawn.unitType = 20; // player
                    spawn.unitInfo.player.characterId = 1;
                    spawn.unitInfo.player.name = "Drakefish";
                    spawn.unitInfo.player.guildName = ""; // temp anti-crash
                    spawn.unitInfo.player.raceId = 12;
                    spawn.unitInfo.player.classId = 4;
                    spawn.unitInfo.player.sex = 0;
                    spawn.unitInfo.player.unkArray = new UnkPlayerStruct0[4]; // temp anti-crash
                    spawn.commandCount = 1; // todo: set a setpos command?
                    spawn.commands = new CreatureCommand[1];
                    spawn.commands[0].type = 2; // setPosition
                    //spawn.commands[0].data.setPosition. 0, 0, 0
                    spawn.factionId = 166;
                    client.SendGamePacketWrapped(spawn);

                    break;
            }
        }

        protected override GamePacketMsg WrapperMsgId
        {
            get { return GamePacketMsg.CClientToGatewayWrapper; }
        }
    }
}
