﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace NxsEmu
{
    public static class Logs
    {
        private static ConcurrentQueue<LogQueueEntry> m_queue = new ConcurrentQueue<LogQueueEntry>();
        private static Thread m_thread;
        private static StreamWriter m_writer;

        static Logs()
        {
            string dir = System.Environment.CurrentDirectory + "\\logs";

            Directory.CreateDirectory(dir);

            m_writer = new StreamWriter(
                File.Open(string.Format("{0}\\{1}.txt", dir,
                DateTime.Now.ToString("yyyyMMddHHmmssffff")), FileMode.Create));

            m_thread = new Thread(() =>
            {
                while (true)
                {
                    LogQueueEntry entry;
                    if (!m_queue.TryDequeue(out entry))
                    {
                        Thread.Sleep(100);
                        continue;
                    }


                    if ((entry.Level & LogLevel.FileOnly) != LogLevel.FileOnly)
                    {
                        if (entry.Level == LogLevel.Error || entry.Level == LogLevel.Exception)
                            Console.ForegroundColor = ConsoleColor.Red;
                        else if (entry.Level == LogLevel.Warning)
                            Console.ForegroundColor = ConsoleColor.Yellow;
                        else
                            Console.ForegroundColor = ConsoleColor.White;

                        Console.WriteLine(entry);
                    }

                    m_writer.WriteLine(entry.ToString());
                    m_writer.Flush();
                }
            });

            m_thread.IsBackground = true;
            m_thread.Start();
        }

        public static void LogBuffer(byte[] buffer)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("");
            sb.AppendLine("|----------------------------------------------------------------|");
            sb.AppendLine("| 00  01  02  03  04  05  06  07  08  09  0A  0B  0C  0D  0E  0F |");
            sb.AppendLine("|----------------------------------------------------------------|");
            sb.Append("|");

            for (int i = 0; i < buffer.Length; i++)
            {
                sb.Append(string.Format(" {0:x2} ", buffer[i].ToString("X2")));

                if (i % 0x10 == 0xF)
                {
                    sb.Append("|");
                    sb.AppendLine();
                    sb.Append("|");
                }
            }

            sb.AppendLine("");
            sb.AppendLine("|----------------------------------------------------------------|");

            Log(LogType.General, LogLevel.Notice, sb.ToString());
        }

        /*public static void LogPacket(Network.PacketStream packet)
        {
            long lastPos = packet.Position;
            packet.Position = 0;
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("");
            sb.AppendLine("|----------------------------------------------------------------|");
            sb.AppendLine("| 00  01  02  03  04  05  06  07  08  09  0A  0B  0C  0D  0E  0F |");
            sb.AppendLine("|----------------------------------------------------------------|");
            sb.Append("|");

            byte[] buffer = new byte[packet.Length];
            packet.Read(buffer, 0, (int)packet.Length);
            
            for (int i = 0; i < buffer.Length; i++)
            {
                sb.Append(string.Format(" {0:x2} ", buffer[i].ToString("X2")));

                if (i % 0x10 == 0xF)
                {
                    sb.Append("|");
                    sb.AppendLine();
                    sb.Append("|");
                }
            }

            sb.AppendLine("");
            sb.AppendLine("|----------------------------------------------------------------|");

            packet.Position = lastPos;
            Log(LogType.Packet, LogLevel.Notice, sb.ToString());
        }*/

        public static void Log(string log, params object[] formatargs)
        {
            Log(string.Format(log, formatargs));
        }

        public static void Log(string log)
        {
            Log(LogType.General, LogLevel.Notice, log);
        }

        public static void Log(LogType type, string log, params object[] formatargs)
        {
            Log(type, string.Format(log, formatargs));
        }

        public static void Log(LogType type, string log)
        {
            Log(type, LogLevel.Notice, log);
        }

        public static void Log(LogType type, LogLevel level, string log, params object[] formatargs)
        {
            Log(type, level, string.Format(log, formatargs));
        }

        public static void Log(LogType type, LogLevel level, string log)
        {
            m_queue.Enqueue(new LogQueueEntry(type, level, log));
        }

        class LogQueueEntry
        {
            public LogQueueEntry(LogType type, LogLevel level, string str)
            {
                Type = type;
                Level = level;
                Text = str;
            }

            public LogType Type { get; set; }
            public LogLevel Level { get; set; }
            public string Text { get; set; }

            public override string ToString()
            {
                if (Type == LogType.None)
                    return Text;
                else
                    return string.Format("[{0}] {1}", Type.ToString(), Text);
            }
        }
    }

    public enum LogType
    {
        /// <summary>
        /// Outputs no "header" in console.
        /// </summary>
        None,
        General,
        DB,
        Sts,
        Auth,
        Realm,
        Packet,
        Network,
        /// <summary>
        /// To be used my runtime compiler services like packet code generator.
        /// </summary>
        Compiler,
    }

    public enum LogLevel
    {
        Notice = 0,
        Warning = 1,
        Error = 2,
        Exception = 3,
        FileOnly = 0x100,
    }
}
