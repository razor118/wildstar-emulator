﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace NxsEmu.Cryptography
{
    /*
     * We need our own to support working with BigInts in the same way the game supports them.
     */ 

    public class SHA256_2
    {
        public const Int32 HASH_OUTPUT_SIZE = 32;

        private static readonly UInt32[] k = new UInt32[64] {
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
            0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
            0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
            0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
            0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
            0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
        };

        // h variable in its original state. (for easy initialization/copying)
        /*private static readonly UInt32[] h_original = new UInt32[8] {
            0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
            0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
        };*/

        private Byte[] data;
        private Int32 datalen;
        private UInt32[] bitlen;
        private UInt32[] state;

        public SHA256_2()
        {
            data = new Byte[64];
            bitlen = new UInt32[2];
            state = new UInt32[8]; /*{
                0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
                0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
            };*/
            state[0] = 0x6a09e667;
            state[1] = 0xbb67ae85;
            state[2] = 0x3c6ef372;
            state[3] = 0xa54ff53a;
            state[4] = 0x510e527f;
            state[5] = 0x9b05688c;
            state[6] = 0x1f83d9ab;
            state[7] = 0x5be0cd19;
        }
        

        /*public static Byte[] Hash(Byte[] buffer)
        {
            Byte[] output = new Byte[HASH_OUTPUT_SIZE];
            Hash(output, buffer, 0, buffer.Length);
            return output;
        }

        public static Byte[] Hash(Byte[] buffer, int offset, int count)
        {
            Byte[] output = new Byte[HASH_OUTPUT_SIZE];
            Hash(output, buffer, offset, count);
            return output;
        }

        /// <summary>
        /// Generates the hash of a the provided buffer and outputs it in the provided output array.
        /// </summary>
        /// <param name="hashOutput"></param>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public static void Hash(Byte[] hashOutput, Byte[] buffer, int offset, int count)
        {
            if (hashOutput == null || buffer == null)
                throw new ArgumentNullException();
            if (hashOutput.Length != HASH_OUTPUT_SIZE)
                throw new ArgumentOutOfRangeException("The hashOutput argument doesn't have the right length.");
            if (buffer.Length < (offset + count))
                throw new ArgumentOutOfRangeException("The provided offset and count arguments go beyond the buffer's length.");
            

        }*/

        public void Transform(Byte[] buffer)
        {
           uint a,b,c,d,e,f,g,h,i,j,t1,t2;
           UInt32[] m = new UInt32[64];
      
           for (i=0,j=0; i < 16; ++i, j += 4)
               m[i] = (UInt32)((buffer[j] << 24) | (buffer[j + 1] << 16) | (buffer[j + 2] << 8) | (buffer[j + 3]));
           for ( ; i < 64; ++i)
              m[i] = SIG1(m[i-2]) + m[i-7] + SIG0(m[i-15]) + m[i-16];

           a = state[0];
           b = state[1];
           c = state[2];
           d = state[3];
           e = state[4];
           f = state[5];
           g = state[6];
           h = state[7];
   
           for (i = 0; i < 64; ++i) {
              t1 = h + EP1(e) + CH(e,f,g) + k[i] + m[i];
              t2 = EP0(a) + MAJ(a,b,c);
              h = g;
              g = f;
              f = e;
              e = d + t1;
              d = c;
              c = b;
              b = a;
              a = t1 + t2;
           }
   
           state[0] += a;
           state[1] += b;
           state[2] += c;
           state[3] += d;
           state[4] += e;
           state[5] += f;
           state[6] += g;
           state[7] += h;
        }

        public void Update(Byte[] buffer, int offset, int count)
        {
            for (uint i = 0; i < count; ++i)
            {
                data[datalen] = buffer[i];
                datalen++;
                if (datalen == 64)
                {
                    Transform(data);
                    DBL_INT_ADD(ref bitlen[0], ref bitlen[1], 512);
                    datalen = 0;
                }
            }
        }

        public void Final(Byte[] outBuffer)
        {
            int i;

            i = datalen;

            // Pad whatever data is left in the buffer. 
            if (datalen < 56)
            {
                data[i++] = 0x80;
                while (i < 56)
                    data[i++] = 0x00;
            }
            else
            {
                data[i++] = 0x80;
                while (i < 64)
                    data[i++] = 0x00;
                Transform(data);
                //memset(data, 0, 56);
                // replaces the memset
                for (int o = 0; o < 56; o++)
                    data[o] = 0;
            }

            // Append to the padding the total message's length in bits and transform. 
            DBL_INT_ADD(ref bitlen[0], ref bitlen[1], datalen * 8);
            data[63] = (Byte)(bitlen[0]);
            data[62] = (Byte)(bitlen[0] >> 8);
            data[61] = (Byte)(bitlen[0] >> 16);
            data[60] = (Byte)(bitlen[0] >> 24);
            data[59] = (Byte)(bitlen[1]);
            data[58] = (Byte)(bitlen[1] >> 8);
            data[57] = (Byte)(bitlen[1] >> 16);
            data[56] = (Byte)(bitlen[1] >> 24);
            Transform(data);

            // Since this implementation uses little endian byte ordering and SHA uses big endian,
            // reverse all the bytes when copying the final state to the output hash. 
            for (i = 0; i < 4; ++i)
            {
                outBuffer[i] = (Byte)((state[0] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 4] = (Byte)((state[1] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 8] = (Byte)((state[2] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 12] = (Byte)((state[3] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 16] = (Byte)((state[4] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 20] = (Byte)((state[5] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 24] = (Byte)((state[6] >> (24 - i * 8)) & 0x000000ff);
                outBuffer[i + 28] = (Byte)((state[7] >> (24 - i * 8)) & 0x000000ff);
            } 
        }



        // Hash functions, try to inline them.

        // DBL_INT_ADD treats two unsigned ints a and b as one 64-bit integer and adds c to it
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void DBL_INT_ADD(ref UInt32 a, ref UInt32 b, Int32 c)
        {
            if (a > 0xffffffff - (c)) ++b; a = (UInt32)(a + c);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 ROTRIGHT(UInt32 a, Int32 b)
        {
            return (((a) >> (b)) | ((a) << (32 - (b))));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 CH(UInt32 x, UInt32 y, UInt32 z)
        {
            return (((x) & (y)) ^ (~(x) & (z)));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 MAJ(UInt32 x, UInt32 y, UInt32 z)
        {
            return (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 EP0(UInt32 x)
        {
            return (ROTRIGHT(x, 2) ^ ROTRIGHT(x, 13) ^ ROTRIGHT(x, 22));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 EP1(UInt32 x)
        {
            return (ROTRIGHT(x, 6) ^ ROTRIGHT(x, 11) ^ ROTRIGHT(x, 25));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 SIG0(UInt32 x)
        {
            return (ROTRIGHT(x, 7) ^ ROTRIGHT(x, 18) ^ ((x) >> 3));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static UInt32 SIG1(UInt32 x)
        {
            return (ROTRIGHT(x, 17) ^ ROTRIGHT(x, 19) ^ ((x) >> 10));
        }
    }
}
