﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NxsEmu.Cryptography
{
    /*
     * I don't know which algorithm this is (or what it's based on)
     * It initializes a 1024bits key from a 64bits key.
     * 
     */

    public class PacketCrypt
    {
        private const Int32 CRYPT_KEY_BITSIZE = 1024;
        private const Int32 CRYPT_KEY_SIZE = CRYPT_KEY_BITSIZE / 8;
        private const UInt32 CRYPT_MULTIPLIER2 = 0xAA7F8EAAu;
        private const UInt64 CRYPT_MULTIPLIER = 0xAA7F8EA9u;
        private const UInt64 CRYPT_KEYVAL_INIT = 0x718DA9074F2DEB91u;
        private readonly Byte[] m_key;
        private readonly UInt64 m_keyVal;

        // This currently works but I haven't tested it in details. See 0.5.12.6373: sub_46CFA0
        // TODO: There may be some more optimization possible in the loop.
        // NOTE: I replaced calls to MultiplyValues() for an actual multiplication. This COULD be wrong.
        public PacketCrypt(UInt64 keyInteger)
        {
            m_key = new Byte[CRYPT_KEY_SIZE];

            UInt64 keyVal = CRYPT_KEYVAL_INIT;
            UInt64 v2 = (keyVal + keyInteger) * CRYPT_MULTIPLIER;
            for (int i = 0; i < CRYPT_KEY_SIZE; i += 8)
            {
                Buffer.BlockCopy(BitConverter.GetBytes(v2), 0, m_key, i, 8);
                keyVal = (keyVal + v2) * CRYPT_MULTIPLIER;
                v2 = (keyInteger + v2) * CRYPT_MULTIPLIER;
            }

            m_keyVal = keyVal;
        }

        public unsafe void DecryptBuffer(Byte[] buffer, int offset, int count)
        {
            if (buffer == null)
                throw new ArgumentNullException("The buffer array can't be null");
            if (buffer.Length < (offset + count))
                throw new ArgumentOutOfRangeException("The provided offset and count arguments go beyond the buffer's length.");

            // this accepts the fact that some caller may not be checking for zero length.
            if (count == 0)
                return;

            Byte* state = stackalloc Byte[8];
            *(UInt64*)state = m_keyVal;

            UInt32 v4 = CRYPT_MULTIPLIER2 * (uint)count;
            UInt32 v9 = 0;

            for (int i = 0; i < count; i++)
            {
                int bufferIndex = i + offset;
                int stateIndex = i % 8;

                if (stateIndex == 0) // each 8 iteration.
                    v9 = (v4++ & 0xF) * 8;

                Byte bufferByte = buffer[bufferIndex];
                buffer[bufferIndex] = (byte)(state[stateIndex] ^ buffer[bufferIndex] ^ m_key[v9 + stateIndex]);
                state[stateIndex] = bufferByte;
            }
        }

        /*
        // will decrypt the offset and output result directly in it instead of allocating a new array.
        public void DecryptBuffer(Byte[] buffer, int offset, int length)
        {
            // 
            Byte[] state = BitConverter.GetBytes(m_keyVal);

            UInt32 v4 = CRYPT_MULTIPLIER2 * (uint)length;
            UInt32 v9 = 0;

            for (int i = 0; i < length; i++)
            {
                int bufferIndex = i + offset;
                int stateIndex = i % 8;

                if (stateIndex == 0) // each 8 iteration.
                    v9 = (v4++ & 0xF) * 8;
                Byte bufferByte = buffer[bufferIndex];
                buffer[bufferIndex] = (byte)(state[stateIndex] ^ buffer[bufferIndex] ^ m_key[v9 + stateIndex]);
                state[stateIndex] = bufferByte;
            }
        }*/

        public Byte[] Decrypt(Byte[] buffer, int length)
        {
            Byte[] outputBytes = new Byte[length];
            Byte[] state = BitConverter.GetBytes(m_keyVal);

            UInt32 v4 = CRYPT_MULTIPLIER2 * (uint)length;
            UInt32 v9 = 0;

            for (int i = 0; i < length; i++)
            {
                int stateIndex = i % 8;

                if (stateIndex == 0) // each 8 iteration.
                    v9 = (v4++ & 0xF) * 8;

                outputBytes[i] = (byte)(state[stateIndex] ^ buffer[i] ^ m_key[v9 + stateIndex]);

                // only difference between encrypt and decrypt
                state[stateIndex] = buffer[i];
            }

            return outputBytes;
        }

        public unsafe void EncryptBufferUnsafe(Byte[] buffer, int offset, int length)
        {
            Byte* state = stackalloc Byte[8];
            *(UInt64*)state = m_keyVal;

            UInt32 v4 = CRYPT_MULTIPLIER2 * (uint)length;
            UInt32 v9 = 0;

            for (int i = 0; i < length; i++)
            {
                int bufferIndex = i + offset;
                int stateIndex = i % 8;

                if (stateIndex == 0) // each 8 iteration.
                    v9 = (v4++ & 0xF) * 8;

                buffer[bufferIndex] = (byte)(state[stateIndex] ^ buffer[bufferIndex] ^ m_key[v9 + stateIndex]);
                state[stateIndex] = buffer[bufferIndex];
            }
        }

        public void EncryptBuffer(Byte[] buffer, int offset, int length)
        {
            Byte[] state = BitConverter.GetBytes(m_keyVal);

            UInt32 v4 = CRYPT_MULTIPLIER2 * (uint)length;
            UInt32 v9 = 0;

            for (int i = 0; i < length; i++)
            {
                int bufferIndex = i + offset;
                int stateIndex = i % 8;

                if (stateIndex == 0) // each 8 iteration.
                    v9 = (v4++ & 0xF) * 8;

                buffer[bufferIndex] = (byte)(state[stateIndex] ^ buffer[bufferIndex] ^ m_key[v9 + stateIndex]);
                state[stateIndex] = buffer[bufferIndex];
            }
        }

        public Byte[] Encrypt(Byte[] buffer, int length)
        {
            Byte[] outputBytes = new Byte[length];
            Byte[] state = BitConverter.GetBytes(m_keyVal);

            UInt32 v4 = CRYPT_MULTIPLIER2 * (uint)length;
            UInt32 v9 = 0;

            for (int i = 0; i < length; i++)
            {
                int stateIndex = i % 8;

                if (stateIndex == 0) // each 8 iteration.
                    v9 = (v4++ & 0xF) * 8;

                outputBytes[i] = (byte)(state[stateIndex] ^ buffer[i] ^ m_key[v9 + stateIndex]);

                // only difference between encrypt and decrypt.
                state[stateIndex] = outputBytes[i];
            }

            return outputBytes;
        }

        public static UInt64 GetKeyFromBuildAndCRC()
        {
            UInt64 key = CRYPT_KEYVAL_INIT + 0x09B107ABC6DB9DAB;
            key = key * CRYPT_MULTIPLIER;
            key = (key + Game.Constants.Build) * CRYPT_MULTIPLIER;
            return (key + Game.Constants.CRC) * CRYPT_MULTIPLIER;
        }

        public static UInt64 GetKeyFromTicket(Byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException();
            if (bytes.Length != 16)
                throw new ArgumentOutOfRangeException();

            UInt64 key = CRYPT_KEYVAL_INIT;

            for (int i = 0; i < 16; i++)
                key = (key + bytes[i]) * CRYPT_MULTIPLIER;

            key = (key + GetKeyFromBuildAndCRC()) * CRYPT_MULTIPLIER;

            return key;
        }
    }
}
