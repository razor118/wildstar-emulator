﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.CompilerServices;
using NxsEmu.Serialization;

namespace NxsEmu.Network.GamePackets
{
    /* GamePacket (abstract): To be used to make packet classes with a specific implementation.
     *  Derived classes must override GetPackedSize, WriteTo and ReadFrom.
     *  Main purpose is to be able to pass packets around without having to worry about their underlying type.
     *  (I currently don't have much use for this class. Maybe for the Wrapper packets?)
     * 
     * GamePacket<TPacket> (abstract): To be used to make packet classes that contain the packet's data definition/structure.
     *  Derived classes don't need to do anything else than contain a set of public nonstatic fields representing the packet's structure.
     *  Must be used with the various Attributes from ./GamePackets/Attributes.
     *  Note: When any derived class gets its type initialized, the static constructor will build and compile the three necessary expressions.
     *        The purpose of this is to make packets building/reading as fast as possible while still making the structure easy to read and change.
     */ 

    public abstract class GamePacket
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Int32 GetPackedByteSize()
        {
            return (GetPackedBitSize() + 7) / 8;
        }

        public abstract GamePacketMsg PacketId { get; }
        public abstract Int32 GetPackedBitSize();
        public abstract void WriteTo(GamePacketWriter writer);
        public abstract void ReadFrom(GamePacketReader reader);
    }

    public abstract class GamePacket<TPacket> : GamePacket
        where TPacket : GamePacket<TPacket>, new()
    {
        private static GamePacketMsg s_packetId;
        private static Action<GamePacketWriter, TPacket> s_writeDelegate;
        private static Action<GamePacketReader, TPacket> s_readDelegate;
        private static Func<TPacket, Int32> s_sizeDelegate;

        static GamePacket()
        {
            PacketDescriptionAttribute attr;
            if ((attr = typeof(TPacket).GetCustomAttribute<PacketDescriptionAttribute>()) == null)
                throw new ApplicationException("Packet doesn't have a PacketDescriptionAttribute.");

            DateTime startTime = DateTime.Now;
            s_packetId = attr.Id;

            GamePacketSerialization.GenerateGamePacketDelegates(
                out s_writeDelegate,
                out s_readDelegate,
                out s_sizeDelegate);

            // all structure expression is done when we reach this point.
            Logs.Log(LogType.Compiler,
                "Generated code for packet {0} in {1} ms",
                s_packetId, (DateTime.Now - startTime).TotalMilliseconds);
        }

        public sealed override GamePacketMsg PacketId
        {
            get { return s_packetId; }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sealed override Int32 GetPackedBitSize()
        {
            return s_sizeDelegate((TPacket)this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sealed override void WriteTo(GamePacketWriter writer)
        {
            s_writeDelegate(writer, (TPacket)this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sealed override void ReadFrom(GamePacketReader reader)
        {
            s_readDelegate(reader, (TPacket)this);
        }
    }
}
