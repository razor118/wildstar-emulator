﻿using System;
using System.Text;
using System.IO;
using System.Runtime.CompilerServices;

namespace NxsEmu.Network.GamePackets
{
    /// <summary>
    /// Reads bit-packed primitive values from a MemoryStream.
    /// </summary>
    public class GamePacketReader : IDisposable
    {
        private const Int32 BYTESIZE = 8;

        private readonly Stream m_stream;
        private readonly Boolean m_leaveOpen;
        private Boolean m_disposed;
        private Byte m_partialByte; // cache of the remaining partial byte.
        private Int32 m_bitsInPartialByte; // number of bits left to read in the overlapped byte field.

        private GamePacketReader() { }
        public GamePacketReader(Stream stream, Boolean leaveOpen = false)
        {
            if (stream == null)
                throw new ArgumentNullException();
            if (!stream.CanRead)
                throw new InvalidOperationException("The stream must be readable.");
            
            m_stream = stream;
            m_leaveOpen = leaveOpen;
            m_partialByte = 0;
            m_bitsInPartialByte = 0;
            // Cache first byte right away.
            CacheByte();
        }

        public Stream BaseStream
        {
            get { return m_stream; }
        }

        protected Byte PartialByte
        {
            get { return m_partialByte; }
            set { m_partialByte = value; }
        }

        protected int BitsInPartialByte
        {
            get { return m_bitsInPartialByte; }
            set { m_bitsInPartialByte = value; }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe SByte ReadSByte(Int32 bits = 8)
        {
            if (bits > sizeof(SByte) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 result = ReadUInt32Unchecked(bits);
            return *(SByte*)&result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe Byte ReadByte(Int32 bits = 8)
        {
            if (bits > sizeof(Byte) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 result = ReadUInt32Unchecked(bits);
            return *(Byte*)&result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Boolean ReadBoolean(Int32 bits = 1)
        {
            if (bits > sizeof(UInt32) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            return ReadUInt32Unchecked(bits) != 0;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe Int16 ReadInt16(Int32 bits = 16)
        {
            if (bits > sizeof(Int16) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 result = ReadUInt32Unchecked(bits);
            return *(Int16*)&result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe UInt16 ReadUInt16(Int32 bits = 16)
        {
            if (bits > sizeof(UInt16) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 result = ReadUInt32Unchecked(bits);
            return *(UInt16*)&result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe Int32 ReadInt32(Int32 bits = 32)
        {
            if (bits > sizeof(Int32) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 result = ReadUInt32Unchecked(bits);
            return *(Int32*)&result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public UInt32 ReadUInt32(Int32 bits = 32)
        {
            if (bits > sizeof(UInt32) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            return ReadUInt32Unchecked(bits);
        }

        public UInt32 ReadUInt32Unchecked(Int32 bits)
        {
            // check for disposed here instead of the publicly exposed readers.
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            uint result = 0;
            int acquiredBits = 0;
            int v17 = 0;
            while (true)
            {
                int remainingBits = 8 - BitsInPartialByte;
                if (remainingBits > bits - acquiredBits)
                    remainingBits = bits - acquiredBits;
                result |= (UInt32)((((1 << remainingBits) - 1) & (PartialByte >> BitsInPartialByte)) << v17);
                acquiredBits = remainingBits + v17;
                v17 += remainingBits;
                if ((BitsInPartialByte = (remainingBits + BitsInPartialByte) & 7) == 0)
                    CacheByte();
                if (acquiredBits >= bits)
                    return result;
            }
        }

        /*
         * Important note:
         *  I think that some (maybe not all) of the Int64 (and possibly others) values have special rules applied to them.
         *  We will have to check this out, we may have to add attribute info or just do extra maths on these.
         */

        public Int64 ReadInt64(Int32 bits = 64)
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            if (bits > sizeof(Int64) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            Int64 result = 0;
            Int32 acquiredBits = 0;
            Int32 v17 = 0;
            while (true)
            {
                int remainingBits = 8 - BitsInPartialByte;
                if (remainingBits > bits - acquiredBits)
                    remainingBits = bits - acquiredBits;
                result |= (Byte)((((1 << remainingBits) - 1) & (PartialByte >> BitsInPartialByte)) << v17);
                acquiredBits = remainingBits + v17;
                v17 += remainingBits;
                if ((BitsInPartialByte = (remainingBits + BitsInPartialByte) & 7) == 0)
                    CacheByte();
                if (acquiredBits >= bits)
                    return result;
            }
        }

        public UInt64 ReadUInt64(Int32 bits = 64)
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            if (bits > sizeof(UInt64) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt64 result = 0;
            Int32 acquiredBits = 0;
            Int32 v17 = 0;
            while (true)
            {
                int remainingBits = 8 - BitsInPartialByte;
                if (remainingBits > bits - acquiredBits)
                    remainingBits = bits - acquiredBits;
                result |= (Byte)((((1 << remainingBits) - 1) & (PartialByte >> BitsInPartialByte)) << v17);
                acquiredBits = remainingBits + v17;
                v17 += remainingBits;
                if ((BitsInPartialByte = (remainingBits + BitsInPartialByte) & 7) == 0)
                    CacheByte();
                if (acquiredBits >= bits)
                    return result;
            }
        }

        public unsafe Single ReadSingle(Int32 bits = 32)
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            if (bits > sizeof(Single) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            UInt32 result = ReadUInt32(bits);
            return *(Single*)&result;
        }

        public unsafe Double ReadDouble(Int32 bits)
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            if (bits > sizeof(Double) * BYTESIZE)
                throw new ArgumentOutOfRangeException();

            // note: this may not be a uint64 (I don't even think this is used actually)
            UInt64 result = ReadUInt64(bits);
            return *(Double*)&result;
        }

        public Byte[] ReadBytesArray(Int32 count, Int32 bits = 8)
        {
            Byte[] bytes = new Byte[count];
            for (int i = 0; i < count; i++)
                bytes[i] = ReadByte(bits);
            return bytes;
        }

        public String ReadWString()
        {
            Byte b = ReadByte(1);
            Int32 count = ReadInt32(b == 0 ? 7 : 15);
            Byte[] bytes = ReadBytesArray(count * 2);
            return Encoding.Unicode.GetString(bytes);
        }

        /// <summary>
        /// Skips the current PartialByte and moves on to the next byte.
        /// Doesn't do anything if there are no used bits in current PartialByte.
        /// </summary>
        public void SkipCurrentByte()
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            if (BitsInPartialByte == 0)
                return;

            CacheByte();
        }

        /// <summary>
        /// Reads and caches the next byte from the BaseStream.
        /// </summary>
        public void CacheByte()
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            int read = BaseStream.ReadByte();

            if (read == -1)
                throw new EndOfStreamException();

            BitsInPartialByte = 0;
            PartialByte = (Byte)read;
        }

        /// <summary>
        /// Re-reads the partial byte but doesn't affect the current Bit Position.
        /// This is necessary when the buffer is modified while being used.
        /// </summary>
        public void ResetCachedByte()
        {
            if (m_disposed)
                throw new ObjectDisposedException("GamePacketReader");

            if (!BaseStream.CanSeek)
                throw new InvalidOperationException(
                    "ResetCachedByte can't complete its work because the stream doesn't support seeking.");

            BaseStream.Seek(-1, SeekOrigin.Current);

            int read = BaseStream.ReadByte();
            if (read == -1)
                throw new EndOfStreamException();

            PartialByte = (Byte)read;
        }

        public void Dispose()
        {
            if (!m_disposed && !m_leaveOpen && BaseStream != null)
                BaseStream.Dispose();

            m_disposed = true;
        }
    }
}
