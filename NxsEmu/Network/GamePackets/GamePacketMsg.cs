﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NxsEmu.Network.GamePackets
{
    public enum GamePacketMsg : ushort
    {
        // Shared
        State = 0,
        State2 = 1,
        SHello = 2,

        // Wrappers
        SGatewayToClientWrapper = 0x294,
        CClientToGatewayWrapper = 0x1B4, // ?
        SAuthToClientWrapper = 0x63,
        CClientToAuthWrapper = 0x1B4, // these are the messages with changed id.

        // Auth C->S
        CAuthRequest = 0x3DF,
        // Auth S->C
        SAuthError = 0x46E,
        SAuthAccept = 0x3DE,
        SGatewayInfo = 0x293,

        // Realm C->S
        CGatewayHello = 0x3DC,
        CGatewayConnect = 0x57D, // possibly a packet it sends when it wants to see characters/realms list.
        CCharacterCreate = 0x1BF,
        CJoinWorld = 0x57A,
        // Realm S->C
        SCharactersList = 0xD9,
        SJoinWorld1 = 0x7E,
        // following are all contained within one packet with others, there may be some missing.
        SJoinWorldUnk2 = 0x5C2,
        SJoinWorldUnk3 = 0xC4,
        SJoinWorldUnk4 = 0x5CA,
        SJoinWorldUnk5 = 0x117,
        SJoinWorldUnk6 = 0xD4,

        SCreatureSpawnInfo = 0x1C6,

    }
}
