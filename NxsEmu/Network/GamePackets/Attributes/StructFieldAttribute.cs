﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [AttributeUsage(AttributeTargets.Field)]
    public class StructFieldAttribute : Attribute
    {
        public StructFieldAttribute() { }
    }
}