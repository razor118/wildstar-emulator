﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [AttributeUsage(AttributeTargets.Field)]
    public class StringFieldAttribute : Attribute
    {
        public StringFieldAttribute(bool unicode) { Unicode = unicode; }
        public bool Unicode { get; private set; }
    }
}
