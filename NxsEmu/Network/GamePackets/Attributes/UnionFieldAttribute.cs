﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class UnionFieldAttribute : Attribute
    {
        public UnionFieldAttribute(string typeFieldName) { TypeFieldName = typeFieldName; }
        public string TypeFieldName { get; private set; }
    }
}
