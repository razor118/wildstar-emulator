﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ArrayFieldAttribute : Attribute
    {
        public ArrayFieldAttribute(string countFieldName) { CountFieldName = countFieldName; }
        public ArrayFieldAttribute(int staticCount) { StaticCount = staticCount; }
        public string CountFieldName { get; private set; }
        public int StaticCount { get; private set; }
    }
}
