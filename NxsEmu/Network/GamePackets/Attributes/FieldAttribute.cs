﻿using System;

namespace NxsEmu.Network.GamePackets
{
    [AttributeUsage(AttributeTargets.Field)]
    public class FieldAttribute : Attribute
    {
        public FieldAttribute(int bits) { Bits = bits; }
        public int Bits { get; private set; }
    }
}
