﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using NxsEmu.Network.GamePackets;
using NxsEmu.Game;

namespace NxsEmu.Network
{
    public abstract class GameServer<TClient> : BasicServer<TClient> 
        where TClient : GameClient
    {
        // TODO: Put these somewhere else? In the GamePacket class maybe?
        private const int PACKET_SIZE_BITS = 0x18; // probably won't change.
        private const int PACKET_SIZE_BYTES = (PACKET_SIZE_BITS + 7) / 8;
        private const int PACKET_ID_BITS = 0xC; // has changed from 0xB over time
        private const int PACKET_ID_BYTES = (PACKET_ID_BITS + 7) / 8;
        private const int PACKET_HEADER_BITS = PACKET_SIZE_BITS + PACKET_ID_BITS;
        private const int PACKET_HEADER_BYTES = (PACKET_HEADER_BITS + 7) / 8;

        public GameServer(ushort port) 
            : base(port) { }

        protected override void ClientConnected(TClient client)
        {
            Logs.Log("GameClient({0}) connected.", typeof(TClient).Name);

            // We should send SHello_Msg here because all GameServers use it.
            // NOTE: It is possible that realm server need some more fields or something.
            // NOTE: AuthServer doesn't encrypt SHello normally but the client still accepts that.
            SHello pkt = new SHello();
            pkt.buildNumber = Constants.Build;
            pkt.networkMessageCRC = Constants.CRC;
            pkt.connectionType = 3; // what's that, does it need to change between auth and realm?
            client.SendGamePacketWrapped(pkt);
        }

        protected sealed override void ReceiveMessage(TClient client, MemoryStream packet)
        {
            using (GamePacketReader reader = new GamePacketReader(packet))
            {
                // NOTE: This currently handles multi-packets but it could probably be improved.
                long remainingPacketBytes = packet.Length;
                long position = 0;
                while (remainingPacketBytes > 0)
                {
                    if (remainingPacketBytes <= PACKET_SIZE_BYTES + 1)
                        throw new ApplicationException();

                    // skip to next byte
                    reader.SkipCurrentByte();

                    UInt32 size = reader.ReadUInt32(PACKET_SIZE_BITS);
                    
                    // TODO: Make our own exception and catch it in BasicServer? Or just log it here and return.
                    if (size > remainingPacketBytes)
                        throw new ApplicationException("TEMP: Packet is too small for read size.");
                    else
                        remainingPacketBytes -= size;

                    UInt32 id = reader.ReadUInt32(PACKET_ID_BITS);
                    GamePacketMsg msg = (GamePacketMsg)id;

                    Logs.Log(LogType.Network, "GameServer received packet size: {0} msg: {1}", size, msg);

                    if (msg == GamePacketMsg.State)
                    {
                        State pkt = new State();
                        pkt.ReadFrom(reader);
                        client.SendGamePacket(pkt);

                        Logs.Log(LogType.Packet, "State MSG: Len: {0} State: {1}", size, pkt.state);
                    }
                    else if (msg == GamePacketMsg.State2)
                    {
                        State2 pkt = new State2();
                        pkt.ReadFrom(reader);
                        client.SendGamePacket(pkt);
                    }
                    else if (msg == WrapperMsgId)
                    {
                        // skip the cached byte, start at next one.
                        reader.SkipCurrentByte();

                        Int32 packedSize = reader.ReadInt32(PACKET_SIZE_BITS) - PACKET_SIZE_BYTES;
                        Int32 packedPos = (int)position + PACKET_HEADER_BYTES + PACKET_SIZE_BYTES;

                        client.PacketCrypt.DecryptBuffer(packet.GetBuffer(), packedPos, packedSize);

                        // reset the cached byte because we just modified the buffer.
                        reader.ResetCachedByte();

                        GamePacketMsg subMsg = (GamePacketMsg)reader.ReadUInt32(PACKET_ID_BITS);
                        DispatchMessage(client, reader, subMsg);
                    }
                    else
                    {
                        DispatchMessage(client, reader, msg);
                    }

                    // increment position (for decryption)
                    position += size;
                }
            }
        }

        // Should return a constant opcode indicating the encrypted wrapper id for this gameserver type. (C->S wrapper Id)
        protected abstract GamePacketMsg WrapperMsgId { get; }
        protected abstract void DispatchMessage(TClient client, GamePacketReader reader, GamePacketMsg msg);
    }
}
