﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NxsEmu.Network.StsCommands
{
    [AttributeUsage(AttributeTargets.Field)]
    class CommandFieldAttribute : Attribute
    {
        private bool m_optional = false;

        public bool Optional
        {
            get { return m_optional; }
            set { m_optional = value; }
        }

        public CommandFieldAttribute() { }
    }
}
