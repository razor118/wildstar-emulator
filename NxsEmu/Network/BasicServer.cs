﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace NxsEmu.Network
{
    /* The BasicServer/BasicClient implementation makes a few guarantees about thread "safety":
     *  
     *  - BasicServer will never pass the same TClient instance to more than a single thread at once.
     *     (if the TClient instance is not shared to other threads by user, it will never be accessed by multiple threads)
     *  - BasicServer re-uses the same buffer for the MemoryStream instance passed to ReceivedMessage
     *      but it doesn't re-use the MemoryStream or the buffer's content so the stream can be modified freely if needed
     *      (there are however no guarantees about the buffer's content beyond MemoryStream's Length property, it's not zero'd)
     *   
     *  
     *  Note:
     *   - For things like RealmServer, it will become necessary to save the TClient instance somewhere and call TClient's SendMessage
     *    from other threads than the current message "handler". I.e. a Player instance would point to a RealmClient so messages can be sent.
     *    It could also be possible that a thread will want to close the TClient.
     *    This means that some thread safety should be added at some level, but where exactly? Maybe the RealmClient should implement the thread safety itself.
     */

    public abstract class BasicServer<TClient>
        where TClient : BasicClient
    {
        private TcpListener m_listener;

        public BasicServer(ushort port)
        {
            // I am using LoopBack instead of Any because I take the IP from here when sending realm address.
            // temp: using Any to test M$ loopback.
            IPAddress ip = IPAddress.Any;//IPAddress.Loopback;
            m_listener = new TcpListener(ip, port);
        }

        public TcpListener TcpListener
        {
            get { return m_listener; }
        }

        public void Start()
        {
            m_listener.Start();
            m_listener.BeginAcceptTcpClient(HandleAsyncConnection, m_listener);
        }

        private void HandleAsyncConnection(IAsyncResult res)
        {
            m_listener.BeginAcceptTcpClient(HandleAsyncConnection, m_listener);
            TcpClient client = m_listener.EndAcceptTcpClient(res);

            if (!IsClientAccepted(client.Client))
            {
                Logs.Log(LogType.Network, LogLevel.Warning, "Refused connection from " + client.Client.RemoteEndPoint);
                return;
            }

            Logs.Log(LogType.Network, "Openned connection from " + client.Client.RemoteEndPoint);

            ServerStateObject state = new ServerStateObject();

            state.client = CreateClient(client.Client);

            ClientConnected(state.client);
            client.Client.BeginReceive(state.buffer, 0, ServerStateObject.BufferSize, SocketFlags.None, HandleAsyncReceive, state);
        }

        private void HandleAsyncReceive(IAsyncResult res)
        {
            ServerStateObject state = (ServerStateObject)res.AsyncState;
            TClient client = state.client;

            try
            {
                // read data from the client socket
                int read = client.Socket.EndReceive(res);

                // data was read from client socket
                if (read > 0)
                {
                    using (MemoryStream stream = new MemoryStream(state.buffer, 0, read, false, true))
                        ReceiveMessage(client, stream);

                    // begin receiving again after handling, so we can re-usse the same stateobject/buffer without issues.
                    client.Socket.BeginReceive(state.buffer, 0, ServerStateObject.BufferSize, 0, HandleAsyncReceive, state);
                }
                else
                {
                    // connection was closed.
                    Logs.Log(LogType.Network, "Client closed connection: {0}", client.Socket.RemoteEndPoint);
                    client.Close();
                }
            }
            catch (SocketException e)
            {
                switch (e.SocketErrorCode)
                {
                    // TODO: More SocketError handling.
                    case SocketError.ConnectionReset:
                        Logs.Log(LogType.Network, LogLevel.Warning,
                            "Client {0} issued a ConnectionReset error.", client);
                        client.Close();
                        break;
                    default:
                        // Swallow the exception, but make sure a log is created and close the client.
                        Logs.Log(LogType.Network, LogLevel.Exception,
                            "Unhandled SocketException (Error:{0}) for client {1} while receiving. Closing the client.",
                            e.SocketErrorCode, client.ToString());
                        client.Close();
                        break;
                }
            }
            catch (Exception e)
            {
                // Note: State for this client may have been corrupted at this point, this is 
                //  an important issue when we reach this handler. This will force process to close.

                // Todo: Close anything else if needed (i.e. session, this will catch every unhandled exception in the handler, ect).
                Logs.Log(LogType.Network, LogLevel.Exception, "Exception occured while handling a packet. {0}\n{1}", e.Message, e.StackTrace);

                // We're most likely crashing, client is going to close anyway,
                //  maybe it's better not to do more with it.
                //client.Close();

                throw;
            }
        }

        protected virtual bool IsClientAccepted(Socket clientSocket)
        {
            return true;
        }

        protected abstract TClient CreateClient(Socket socket);
        protected abstract void ClientConnected(TClient client);
        protected abstract void ReceiveMessage(TClient client, MemoryStream packet);

        // See: http://msdn.microsoft.com/en-us/library/5w7b7x5f.aspx
        private class ServerStateObject
        {
            public TClient client;
            // TODO/NOTE: This is the size used by client, should we use another?
            public const int BufferSize = 1024;//0x1FFFE;
            public byte[] buffer = new byte[BufferSize];
        }
    }
}
