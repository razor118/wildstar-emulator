﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace NxsEmu.Network
{
    public abstract class BasicClient
    {
        private Socket m_socket;

        public Socket Socket
        {
            get { return m_socket; }
            protected set { m_socket = value; }
        }

        public BasicClient(Socket socket)
        {
            Socket = socket;
        }

        public void Close()
        {
            Socket.Close(0);
            // TODO: we may want to code a close "notice" dispatcher. Some servers may
            //  want to be notified as soon as the client is closed, to remove a unit from world, by example.
        }

        public void SendMessage(MemoryStream stream)
        {
            // we're using the stream's buffer, is this thread-safe for whatever may be re-using the buffer after?
            byte[] buffer = stream.GetBuffer();
            Socket.BeginSend(buffer, 0, (int)stream.Length, SocketFlags.None, new AsyncCallback(HandleAsyncSend), this);
        }

        private static void HandleAsyncSend(IAsyncResult res)
        {
            BasicClient client = (BasicClient)res.AsyncState;

            try
            {
                int bytesSent = client.Socket.EndSend(res);

                //Logs.Log(LogType.Network, "Sent {0} bytes to client {1}", bytesSent, client.Socket.RemoteEndPoint);
            }
            catch (SocketException e)
            {
                // Swallow the exception, but make sure a log is created and close the client.
                Logs.Log(LogType.Network, LogLevel.Exception,
                    "Unhandled SocketException (Error:{0}) for client {1} while sending. Closing the client.",
                    e.SocketErrorCode, client);
                client.Close();
            }
            catch (Exception e)
            {
                Logs.Log(LogType.Network, LogLevel.Exception, "Failed to send message to a client. Exception: {0}\n{1}", e.Message, e.Source);

                // don't swallow, this is a bug.
                throw;
            }
        }

        public override string ToString()
        {
            if (Socket == null)
                return "Invalid BaseClient";

            return Socket.RemoteEndPoint.ToString();
        }
    }
}
