﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NxsEmu.Game
{
    public static class Constants
    {
        // These aren't exactly constants, maybe put somewhere else eventually.
        public readonly static UInt32 Build = 6395;
        public readonly static UInt32 CRC = 0x830ADC69;

    }
}
