﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NxsEmu.Network.GamePackets;

namespace NxsEmu.Game
{
    public struct WorldLocation
    {
        [StructField()]
        public Position location;
        [Field(32)]
        public float yaw;
        [Field(32)]
        public float pitch;
        [Field(32)]
        public uint tileId;
    }

    // todo: make our own Vector3 implementation use this.
    public struct Position
    {
        [Field(32)]
        public float x;
        [Field(32)]
        public float y;
        [Field(32)]
        public float z;
    }
}
