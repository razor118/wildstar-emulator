﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;

namespace NxsEmu.DataTables
{
    // the base for all structures. Contains the id which is present in all table structs.
    public abstract class DataTableEntry<T>
        where T : DataTableEntry<T>
    {
        // Emu code shouldn't support writing at all.
        // temporary delegates, will probably change.
        //private delegate void WriteDelegate(BinaryWriter writer, T[] rows);
        private delegate void ReadDelegate(BinaryReader writer, T[] rows);

        //private static readonly WriteDelegate s_writeDelegate;
        private static readonly ReadDelegate s_readDelegate;

        [DataTblField(0)]
        public readonly UInt32 id;

        // static ctor for this basically creates the (de)serialization for fields.
        // It will either load all rows to a specified array or provide a func for each row.
        // I prefer the first option.
        static DataTableEntry()
        {
            DynamicMethod method = new DynamicMethod(
                "WriteDataTableEntries",
                typeof(void),
                new[] { typeof(BinaryWriter), typeof(T[]) });

            ILGenerator il = method.GetILGenerator();

            //il.Emit(OpCodes.Ldarg_0);
            //il.Emit(

            // loop over fields, generate reader for them.
            // we need to allow unused fields somehow (attributes), but unless we use offsets we
            //  still need to consider these if we want to make a linear read of the file.
            // Another option which is possible with Emit is to use pointers, but we would need to make
            //  sure that manipulation is safe via some checks of the file (maybe add some GetRowSize like our packet stuff?)
        }
    }
}
